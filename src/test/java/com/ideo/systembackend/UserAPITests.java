package com.ideo.systembackend;

import com.ideo.systembackend.datamodel.DigitalCitizen;
import com.ideo.systembackend.datamodel.DigitalCitizenDelegate;
import com.ideo.systembackend.datamodel.REST.RequestData;
import com.ideo.systembackend.service.DatabaseService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureWebTestClient
public class UserAPITests {

    @Autowired
    private WebTestClient client;

    @Autowired
    private DatabaseService databaseService;

    @Test
    public void createUser_Success() {
        client.post().uri("/user/loginOrCreate")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .body(BodyInserters.fromObject(new RequestData("testaccount", "admin", "0142")))
                .exchange()
                .expectStatus().isOk()
                .expectBody().returnResult();
    }

    @Test
    public void findAdminUserAfterCreate_Success() {
        RequestData data = new RequestData("testuser", "admin", "");
        databaseService.getOrCreateCitizen(data)
                .subscribe(s -> {
                    data.setAdminCode(s.getAdminCode());
                    client.post().uri("/user/loginOrCreate")
                            .contentType(MediaType.APPLICATION_JSON_UTF8)
                            .accept(MediaType.APPLICATION_JSON_UTF8)
                            .body(BodyInserters.fromObject(data))
                            .exchange()
                            .expectStatus().isOk()
                            .expectBody()
                            .jsonPath("$.citizenId").isEqualTo(s.getCitizenId())
                            .jsonPath("$.citizenType").isEqualTo(s.getCitizenType())
                            .jsonPath("$.adminCode").isEqualTo(s.getAdminCode())
                            .jsonPath("$.username").isEqualTo(s.getUsername());
                });
    }


    @Test
    public void createUser_Failure(){
        RequestData data = new RequestData("testuser", "admin", "");
//        client.post().uri("/user/getOrCreate")
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .accept(MediaType.APPLICATION_JSON_UTF8)
//                .body(BodyInserters.fromObject(data))
//                .exchange()
//                .expectStatus().is4xxClientError()
//                .expectBody().returnResult();
    }


}
