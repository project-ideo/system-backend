package com.ideo.systembackend.repository;

import com.ideo.systembackend.datamodel.DigitalCitizen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DCitizenRepo extends JpaRepository<DigitalCitizen, String> {

    public DigitalCitizen findDigitalCitizenByUsername(String username);
}
