package com.ideo.systembackend.service;

import com.ideo.systembackend.datamodel.DigitalCitizen;
import com.ideo.systembackend.datamodel.DigitalCitizenDelegate;
import com.ideo.systembackend.datamodel.REST.RequestData;
import com.ideo.systembackend.repository.DCitizenRepo;
import org.apache.commons.text.RandomStringGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Date;
import java.util.UUID;

@Service
public class DatabaseService {

    @Autowired
    private DCitizenRepo citizenRepo;

    private RandomStringGenerator randomString;

    public DatabaseService() {
        randomString = new RandomStringGenerator.Builder().withinRange('a','z').build();
    }

    public Mono<DigitalCitizenDelegate> getOrCreateCitizen(RequestData data){
        return Mono.justOrEmpty(citizenRepo.findDigitalCitizenByUsername(data.getUsername()))
                .flatMap(s -> {
                    if(s.getCitizenType().equalsIgnoreCase("admin")){
                        return s.getAdminCode().equals(data.getAdminCode()) ? Mono.just(s) : Mono.error(new Exception("Admin passcode is wrong."));
                    }
                    else {
                        return Mono.just(s);
                    }
                })
                .flatMap(s -> Mono.just(new DigitalCitizenDelegate(s.getCitizenId(), s.getCitizenType(), s.getUsername(), s.getAdminCode(), s.getLastLoginDate(), s.getCreatedDate())))
                .switchIfEmpty(createUser(data));
    }

    public Mono<DigitalCitizenDelegate> getNormalUser(String username){
        return Mono.justOrEmpty(citizenRepo.findDigitalCitizenByUsername(username))
                .map(s -> new DigitalCitizenDelegate(s.getCitizenId(), s.getCitizenType(), s.getUsername(), s.getAdminCode(), s.getLastLoginDate(), s.getCreatedDate()));
    }


    private Mono<DigitalCitizenDelegate> createUser(RequestData data){
        return Mono.just(new DigitalCitizen(data.getUsername(), data.getCitizenType()))
                .doOnNext(s -> {
                    if(data.getCitizenType().equalsIgnoreCase("admin"))
                        s.setAdminCode(randomString.generate(4));
                    s.setCitizenId(UUID.randomUUID().toString());
                    s.setCitizenType(data.getCitizenType());
                    s.setLastLoginDate(new Date());
                    s.setCreatedDate(new Date());

                })
                .flatMap(s -> Mono.just(citizenRepo.save(s)))
                .flatMap(s -> Mono.just(new DigitalCitizenDelegate(s.getCitizenId(), s.getCitizenType(), s.getUsername(), s.getAdminCode(), s.getLastLoginDate(), s.getCreatedDate())));
    }

    public Mono<DigitalCitizenDelegate> save(DigitalCitizen citizen){
        return Mono.just(citizenRepo.save(citizen))
                .map(s -> new DigitalCitizenDelegate(s.getCitizenId(), s.getCitizenType(), s.getUsername(), s.getAdminCode(), s.getLastLoginDate(), s.getCreatedDate()));
    }


}
