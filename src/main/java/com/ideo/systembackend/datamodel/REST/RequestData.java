package com.ideo.systembackend.datamodel.REST;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestData {

    private String username;
    private String citizenType;
    private String adminCode;
}
