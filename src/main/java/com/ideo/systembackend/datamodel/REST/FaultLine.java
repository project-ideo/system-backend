package com.ideo.systembackend.datamodel.REST;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FaultLine {

    private String status;
    private String message;
    private Object data;

    public FaultLine(String status, String message) {
        this.status = status;
        this.message = message;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
