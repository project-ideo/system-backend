package com.ideo.systembackend.datamodel;

import com.google.gson.Gson;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "digital_citizen")
public class DigitalCitizen {

    @Id
    private String citizenId;

    @Column(length = 10, nullable = false)
    private String citizenType;

    @Column(length = 64, unique = true, nullable = false)
    private String username;

    @Column(length = 4, unique = true)
    private String adminCode;

    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLoginDate;

    @Temporal(TemporalType.TIME)
    private Date createdDate;

    public DigitalCitizen(String username, String citizenType) {
        this.citizenType = citizenType;
        this.username = username;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
