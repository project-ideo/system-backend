package com.ideo.systembackend.datamodel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DigitalCitizenDelegate {

    private String citizenId;
    private String citizenType;
    private String username;
    private String adminCode;
    private Date lastLoginDate;
    private Date createdDate;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
