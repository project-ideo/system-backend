package com.ideo.systembackend.controller;

import com.ideo.systembackend.datamodel.DigitalCitizenDelegate;
import com.ideo.systembackend.datamodel.REST.FaultLine;
import com.ideo.systembackend.datamodel.REST.RequestData;
import com.ideo.systembackend.service.DatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import static org.springframework.http.ResponseEntity.ok;

@Controller
@RequestMapping("/user")
@CrossOrigin(value = {"http://localhost:3000"})
public class UserController {

    @Autowired
    private DatabaseService service;


    @GetMapping("/user/{username}/login")
    public Mono<ResponseEntity> normalLoginAPI(@PathVariable String username){
        return service.getNormalUser(username)
                .flatMap(s -> Mono.just(ok(s)));
    }




    @PostMapping("/loginOrCreate")
    public Mono<ResponseEntity<DigitalCitizenDelegate>> getOrCreateUser(@RequestBody RequestData data){
        return service.getOrCreateCitizen(data)
                .flatMap(s -> Mono.just(ok(s)))
                .doOnError(s -> Mono.just(new FaultLine("Failed", s.getMessage())));
    }
}
